package com.mlaguna.tienda;

import com.mlaguna.tienda.gui.Controlador;
import com.mlaguna.tienda.gui.Modelo;
import com.mlaguna.tienda.gui.Vista;

public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
