package com.mlaguna.tienda.gui;

import com.mlaguna.tienda.base.Juego;
import com.mlaguna.tienda.base.Pelicula;
import com.mlaguna.tienda.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Modelo {
    private final static String COLECCION_JUEGOS = "Juegos";
    private final static String COLECCION_PELICULAS = "Peliculas";
    private final static String DATABASE = "Tienda";

    private MongoClient cliente;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionJuegos;
    private MongoCollection coleccionPeliculas;

    /**
     * Metodo que conecta a la base de datos y a sus colecciones
     */
    public void conectar(){
        cliente = new MongoClient();
        baseDatos = cliente.getDatabase(DATABASE);
        coleccionJuegos = baseDatos.getCollection(COLECCION_JUEGOS);
        coleccionPeliculas = baseDatos.getCollection(COLECCION_PELICULAS);
    }

    /**
     * Desconecta la base de datos
     */
    public void desconectar(){
        cliente.close();
    }

    /**
     * Metodo para guardar un juego e insertarlo en la coleccion como documento
     * @param juego
     */
    public void guardarJuego(Juego juego){
        coleccionJuegos.insertOne(juegoToDocument(juego));
    }

    /**
     * Metodo para guardar una pelicula e insertarlo en la coleccion como documento
     * @param pelicula
     */
    public void guardarPelicula(Pelicula pelicula){
        coleccionPeliculas.insertOne(peliculaToDocument(pelicula));
    }

    /**
     * Lista la lista de juegos
     * @return
     */
    public List<Juego> getJuegos(){
        ArrayList<Juego> lista = new ArrayList<>();
        Iterator<Document> it = coleccionJuegos.find().iterator();
        while(it.hasNext()){
            lista.add(documentToJuego(it.next()));
        }
        return lista;
    }

    /**
     * Lista la lista de peliculas
     * @return
     */
    public List<Pelicula> getPeliculas(){
        ArrayList<Pelicula> lista = new ArrayList<>();
        Iterator<Document> it = coleccionPeliculas.find().iterator();
        while(it.hasNext()){
            lista.add(documentToPelicula(it.next()));
        }
        return lista;
    }

    /**
     * Lista los juegos a la hora de buscarlo mediante criterios
     * @param text criterio a buscar
     * @return
     */
    public List<Juego> getJuegos(String text){
        ArrayList<Juego> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("titulo", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("precio", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("fechaLanzamiento", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> it = coleccionJuegos.find(query).iterator();
        while(it.hasNext()){
            lista.add(documentToJuego(it.next()));
        }
        return lista;
    }

    /**
     * Lista las peliculas a la hora de buscarla mediante criterios
     * @param text criterio a buscar
     * @return
     */
    public List<Pelicula> getPeliculas(String text){
        ArrayList<Pelicula> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("titulo", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("precio", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("fechaLanzamiento", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> it = coleccionPeliculas.find(query).iterator();
        while(it.hasNext()){
            lista.add(documentToPelicula(it.next()));
        }
        return lista;
    }

    /**
     * Convierte los datos de un juego a un documento para la coleccion
     * @param juego
     * @return
     */
    private Document juegoToDocument(Juego juego) {
        Document documento = new Document();
        documento.append("titulo", juego.getTitulo());
        documento.append("precio", juego.getPrecio());
        documento.append("fechaLanzamiento", Util.formatearFecha(juego.getFechaLanzamiento()));
        return documento;
    }

    /**
     * Convierte los datos de una pelicula a un documento para la coleccion
     * @param pelicula
     * @return
     */
    private Document peliculaToDocument(Pelicula pelicula) {
        Document documento = new Document();
        documento.append("titulo", pelicula.getTitulo());
        documento.append("precio", pelicula.getPrecio());
        documento.append("fechaLanzamiento", Util.formatearFecha(pelicula.getFechaLanzamiento()));
        return documento;
    }

    /**
     * Convierte un documento de la coleccion a un Juego
     * @param document
     * @return
     */
    private Juego documentToJuego(Document document){
        Juego juego = new Juego();
        juego.setId(document.getObjectId("_id"));
        juego.setTitulo(document.getString("titulo"));
        juego.setPrecio(document.getDouble("precio"));
        juego.setFechaLanzamiento(Util.parsearFecha(document.getString("fechaLanzamiento")));
        return juego;
    }

    /**
     * Convierte un documento de la coleccion a Pelicula
     * @param document
     * @return
     */
    private Pelicula documentToPelicula(Document document){
        Pelicula pelicula = new Pelicula();
        pelicula.setId(document.getObjectId("_id"));
        pelicula.setTitulo(document.getString("titulo"));
        pelicula.setPrecio(document.getDouble("precio"));
        pelicula.setFechaLanzamiento(Util.parsearFecha(document.getString("fechaLanzamiento")));
        return pelicula;
    }

    /**
     * Metodo que modifica los datos en la base de datos
     * @param juego
     */
    public void modificarJuego(Juego juego){
        coleccionJuegos.replaceOne(new Document("_id", juego.getId()), juegoToDocument(juego));
    }

    /**
     * MEtodo que modifica los datos en la base de datos
     * @param pelicula
     */
    public void modificarPelicula(Pelicula pelicula){
        coleccionPeliculas.replaceOne(new Document("_id", pelicula.getId()), peliculaToDocument(pelicula));
    }

    /**
     * Metodo que borra un juego en la base de datos
     * @param juego
     */
    public void borrarJuego(Juego juego){
        coleccionJuegos.deleteOne(juegoToDocument(juego));
    }

    /**
     * Metodo que borra una pelicula a la base de datos
     * @param pelicula
     */
    public void borrarPelicula(Pelicula pelicula){
        coleccionPeliculas.deleteOne(peliculaToDocument(pelicula));
    }

}