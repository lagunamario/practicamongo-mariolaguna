package com.mlaguna.tienda.gui;

import com.mlaguna.tienda.base.Juego;
import com.mlaguna.tienda.base.Pelicula;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.util.List;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener, MouseListener {
    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;

        init();
    }

    /**
     * Metodo para iniciar los listener, conectar la base de datos y actualizar las listas
     */
    private void init(){
        addActionListener(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        addMouseListener(this);
        modelo.conectar();
        listarJuegos(modelo.getJuegos());
        listarPeliculas(modelo.getPeliculas());
    }

    /**
     * Inicializa los MouseListener
     * @param listener
     */
    private void addMouseListener(MouseListener listener) {
        vista.listJuegos.addMouseListener(listener);
        vista.listPeliculas.addMouseListener(listener);
    }

    /**
     * Inicializa los ListSelectionListener
     * @param listener
     */
    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.listJuegos.addListSelectionListener(listener);
        vista.listPeliculas.addListSelectionListener(listener);
    }

    /**
     * Inicializa los KeyListener
     * @param listener
     */
    private void addKeyListeners(KeyListener listener) {
        vista.txtBuscarJuego.addKeyListener(listener);
    }

    /**
     * Inicializa los ActionListener
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.btnAddJuego.addActionListener(listener);
        vista.btnModificarJuego.addActionListener(listener);
        vista.btnEliminarJuego.addActionListener(listener);

        vista.btnAddPelicula.addActionListener(listener);
        vista.btnModificarPelicula.addActionListener(listener);
        vista.btnEliminarPelicula.addActionListener(listener);
    }

    /**
     * Switch - case que coge el Action Command de cada boton y depende de cual sea, realiza una funcion u otra
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Juego juego;
        Pelicula pelicula;
        switch(comando){
            case "Nuevo Juego":
                juego = new Juego();
                setDatosJuego(juego);
                modelo.guardarJuego(juego);
                listarJuegos(modelo.getJuegos());
                limpiarCamposJuego();
                break;
            case "Modificar Juego":
                juego = (Juego) vista.listJuegos.getSelectedValue();
                setDatosJuego(juego);
                modelo.modificarJuego(juego);
                listarJuegos(modelo.getJuegos());
                limpiarCamposJuego();
                break;
            case "Eliminar Juego":
                juego = (Juego) vista.listJuegos.getSelectedValue();
                modelo.borrarJuego(juego);
                listarJuegos(modelo.getJuegos());
                limpiarCamposJuego();
                break;

            case "Nueva Pelicula":
                pelicula = new Pelicula();
                setDatosPelicula(pelicula);
                modelo.guardarPelicula(pelicula);
                listarPeliculas(modelo.getPeliculas());
                limpiarCamposPelicula();
                break;
            case "Modificar Pelicula":
                pelicula = (Pelicula) vista.listPeliculas.getSelectedValue();
                setDatosPelicula(pelicula);
                modelo.modificarPelicula(pelicula);
                listarPeliculas(modelo.getPeliculas());
                limpiarCamposPelicula();
                break;
            case "Eliminar Pelicula":
                pelicula = (Pelicula) vista.listPeliculas.getSelectedValue();
                modelo.borrarPelicula(pelicula);
                listarPeliculas(modelo.getPeliculas());
                limpiarCamposPelicula();
                break;
        }

    }

    /**
     * Limpia los campos de texto de la coleccion de juegos
     */
    private void limpiarCamposJuego() {
        vista.txtTituloJuego.setText("");
        vista.txtPrecioJuego.setText("");
        vista.dpFechaLanzamientoJuego.setDate(null);
    }

    /**
     * Limpia los campos de texto de la coleccion de peliculas
     */
    private void limpiarCamposPelicula() {
        vista.txtTituloPelicula.setText("");
        vista.txtPrecioPelicula.setText("");
        vista.dpFechaLanzamientoPelicula.setDate(null);
    }

    /**
     * Muestra los campos de la coleccion juegos cuando se selecciona
     */
    private void mostrarCamposJuego(){
        Juego juego = (Juego) vista.listJuegos.getSelectedValue();
        vista.txtTituloJuego.setText(juego.getTitulo());
        vista.txtPrecioJuego.setText(String.valueOf(juego.getPrecio()));
        vista.dpFechaLanzamientoJuego.setDate(juego.getFechaLanzamiento());
    }

    /**
     * Muestra los campos de la coleccion peliculas cuando se selecciona
     */
    private void mostrarCamposPelicula(){
        Pelicula pelicula = (Pelicula) vista.listPeliculas.getSelectedValue();
        vista.txtTituloPelicula.setText(pelicula.getTitulo());
        vista.txtPrecioPelicula.setText(String.valueOf(pelicula.getPrecio()));
        vista.dpFechaLanzamientoPelicula.setDate(pelicula.getFechaLanzamiento());
    }

    /**
     * Metodo que sirve para que a la hora de agregar un juego se haga con los datos de los campos de texto
     * @param juego
     */
    private void setDatosJuego(Juego juego) {
        juego.setTitulo(vista.txtTituloJuego.getText());
        juego.setPrecio(Double.parseDouble(vista.txtPrecioJuego.getText()));
        juego.setFechaLanzamiento(vista.dpFechaLanzamientoJuego.getDate());
    }

    /**
     * Metodo que sirve para que a la hora de agregar una pelicula se haga con los datos de los campos de texto
     * @param pelicula
     */
    private void setDatosPelicula(Pelicula pelicula) {
        pelicula.setTitulo(vista.txtTituloPelicula.getText());
        pelicula.setPrecio(Double.parseDouble(vista.txtPrecioPelicula.getText()));
        pelicula.setFechaLanzamiento(vista.dpFechaLanzamientoPelicula.getDate());
    }

    /**
     * Actualiza la lista de juegos
     * @param lista
     */
    private void listarJuegos(List<Juego> lista){
        vista.dlmJuegos.clear();
        for (Juego juego : lista) {
            vista.dlmJuegos.addElement(juego);
        }
    }

    /**
     * Actualiza la lista de peliculas
     * @param lista
     */
    private void listarPeliculas(List<Pelicula> lista){
        vista.dlmPelicula.clear();
        for (Pelicula pelicula : lista) {
            vista.dlmPelicula.addElement(pelicula);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que recoge la fuente del campo de texto de buscar un juego cuando la aplicacion detecta que has liberado
     * una tecla y realiza el listado de las colecciones
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarJuego){
            listarJuegos(modelo.getJuegos(vista.txtBuscarJuego.getText()));
        }
        else if (e.getSource() == vista.txtBuscarPelicula){
            listarPeliculas(modelo.getPeliculas(vista.txtBuscarPelicula.getText()));
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        /*if(e.getSource() == vista.listJuegos) {
            Juego juego = (Juego) vista.listJuegos.getSelectedValue();
            vista.txtTituloJuego.setText(juego.getTitulo());
            vista.txtPrecioJuego.setText(String.valueOf(juego.getPrecio()));
            vista.dpFechaLanzamientoJuego.setDate(juego.getFechaLanzamiento());
            mostrarCamposJuego();
        }
        else if (e.getSource() == vista.listPeliculas){
            Pelicula pelicula = (Pelicula) vista.listPeliculas.getSelectedValue();
            vista.txtTituloPelicula.setText(pelicula.getTitulo());
            vista.txtPrecioPelicula.setText(String.valueOf(pelicula.getPrecio()));
            vista.dpFechaLanzamientoPelicula.setDate(pelicula.getFechaLanzamiento());
            mostrarCamposPelicula();
        }*/
    }

    /**
     * Metodo que recoge la fuente de la lista de juegos y de peliculas y detecta cuando le has hecho click con el raton haciendo que
     * se muestren los datos en los campos de texto
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.listJuegos){
            mostrarCamposJuego();
        }
        else if (e.getSource() == vista.listPeliculas){
            mostrarCamposPelicula();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
