package com.mlaguna.tienda.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.mlaguna.tienda.base.Juego;
import com.mlaguna.tienda.base.Pelicula;

import javax.swing.*;

public class Vista extends JFrame{
    private JPanel panel1;

    //Campos coleccion juego
    JTextField txtTituloJuego;
    JTextField txtPrecioJuego;
    DatePicker dpFechaLanzamientoJuego;
    JButton btnAddJuego;
    JButton btnModificarJuego;
    JButton btnEliminarJuego;
    JList listJuegos;
    JTextField txtBuscarJuego;

    //Campos coleccion pelicula
    JButton btnAddPelicula;
    JButton btnModificarPelicula;
    JButton btnEliminarPelicula;
    JList listPeliculas;
    JTextField txtTituloPelicula;
    JTextField txtPrecioPelicula;
    JTextField txtBuscarPelicula;
    DatePicker dpFechaLanzamientoPelicula;

    DefaultListModel<Juego> dlmJuegos;
    DefaultListModel<Pelicula> dlmPelicula;

    public Vista() {
        this.setTitle("Practica de Mongo");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);

        inicializar();
    }

    /**
     * Metodo que inicializa los DLM y se los aplica a los JList
     */
    private void inicializar() {
        dlmJuegos = new DefaultListModel<>();
        dlmPelicula = new DefaultListModel<>();
        listJuegos.setModel(dlmJuegos);
        listPeliculas.setModel(dlmPelicula);

        dpFechaLanzamientoJuego.getComponentToggleCalendarButton().setText("Dat");
        dpFechaLanzamientoPelicula.getComponentToggleCalendarButton().setText("Dat");
    }
}
