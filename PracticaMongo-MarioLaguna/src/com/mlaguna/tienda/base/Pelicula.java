package com.mlaguna.tienda.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Pelicula {
    private ObjectId id;
    private String titulo;
    private double precio;
    private LocalDate fechaLanzamiento;

    public Pelicula(){

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public LocalDate getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    public void setFechaLanzamiento(LocalDate fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    @Override
    public String toString() {
        return
                "Titulo='" + titulo + '\'' +
                ", precio=" + precio +
                ", fechaLanzamiento=" + fechaLanzamiento;
    }
}
